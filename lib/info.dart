import 'dart:html';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:knowledge_wiki/drawer.dart';
import 'package:url_launcher/url_launcher.dart';


class InfoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      drawer: WikiDrawer(page: Page.info),
      appBar: AppBar(
        title: Text("Knowledge-Wiki - Information"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            InkWell(
              child:
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(50.0),
                  child: Column(children: <Widget>[
                    Text(
                      "Information about this open source project",
                      style: Theme.of(context).textTheme.title,
                    ),
                    Text(
                      "GitLab: https://gitlab.com/knowledge-wiki \r\n"
                          "MIT License \r\n"
                          "Contributors: CodeDoctorDE\r\n"
                          "(C) 2019",

                    ),
                  ]),
                ),
              ),
              onTap: () {
                print("Test");
                window.open("https://gitlab.com/knowledge-wiki",
                    "GitLab - Knowledge-Wiki");
                launch("https://gitlab.com/knowledge-wiki");
              },
            ),
          ],
        ),
      ),
    );
  }
}
